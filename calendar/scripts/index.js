// 2021
const year = 2021;
const months = [
	{
		name: "January",
		len: 31,
		starts: 4
	},
	{
		name: "February",
		len: 28,
		starts: 0
	},
	{
		name: "March",
		len: 31,
		starts: 0
	},
	{
		name: "April",
		len: 30,
		starts: 3
	},
	{
		name: "May",
		len: 31,
		starts: 5
	},
	{
		name: "June",
		len: 30,
		starts: 1
	},
	{
		name: "July",
		len: 31,
		starts: 3
	},
	{
		name: "August",
		len: 31,
		starts: 6
	},
	{
		name: "September",
		len: 30,
		starts: 2
	},
	{
		name: "October",
		len: 31,
		starts: 4
	},
	{
		name: "November",
		len: 30,
		starts: 0
	},
	{
		name: "December",
		len: 31,
		starts: 2
	}
];
let monthNum = 7;
let month = months[monthNum];

// Set a different image for each month in the side oval... 
let imageIndexString = ("" + monthNum).padStart(2, '0');
let imageFilepath = `images/side_img${imageIndexString}.png`;
document.getElementById("side-image").style.backgroundImage = `url(${imageFilepath})`;

document.getElementById("month-name").textContent = `${month.name} ${year}`;

for (let dayNum = 1; dayNum <= month.len; dayNum++) {
	let boxNum = dayNum + month.starts - 1;
	let day = document.querySelectorAll(".day")[boxNum];
	day.textContent = dayNum;
	// create some nice shades...
	let r = 255 - Math.floor(Math.random() * 96);
	let g = 255 - Math.floor(Math.random() * 160);
	let b = 255 - Math.floor(Math.random() * 64);
	let dayOfWeekNum = boxNum % 7;
	let isWeekend = dayOfWeekNum == 5 || dayOfWeekNum == 6;
	if (isWeekend) {
		// make the weekend redder...
		[g, b] = [g-128, b-96];
	}

	// I do love the new ES6 string interpolation!   And backticks have hitherto been generally underused!
	day.style.backgroundImage = `radial-gradient(circle at top right, rgb(${b}, ${g}, ${r}), rgb(${isWeekend ? r : 255-r}, ${isWeekend ? g : 255-g}, ${isWeekend ? b : 255-b}))`;
	

	// Now let's make the corners wacky!...
	let tl = Math.random() * 90 + 10;
	let bl = Math.random() * 160; // 90 + 10;
	let br = Math.random() * 90 + 10;
	day.style.borderRadius = `${tl}% 7% ${bl}% ${br}%`; // keeping top-right fixed as the number is there
	day.style.borderStyle = "1px solid #0099ff";
}

// for (let boxNum = 0; boxNum < 35; boxNum++) {
// 	let box = document.querySelectorAll(".day")[boxNum];
// 	if (boxNum < month.starts || boxNum > month.starts + month.len - 1) {
// 		box.style.borderStyle = "none";
// 	}
