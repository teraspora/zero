const input = document.querySelector("input");
const output = document.querySelector("output");
const canvas = document.getElementById(`canv`);
const ctx = canvas.getContext(`2d`);
const images = [];
const repeat_button = document.querySelector("button#repeat");
const back_button = document.querySelector("button#back");

[canvas.width, canvas.height] = [canvas.parentElement.clientWidth * 0.8, canvas.parentElement.clientHeight * 0.9];

input.addEventListener("change", _ => {
    const img = new Image();
    img.src = URL.createObjectURL(input.files[0]);
    images.push(img);
    display_image(img);
  });

repeat_button.addEventListener('click', _ => {
    const new_img = new Image();
    new_img.src = canvas.toDataURL();
    images.push(new_img);
    display_image(new_img);
});

back_button.addEventListener('click', _ => {
    if (images.length) {
        const old_img = images.pop();
        draw(old_img);
    }
});

function display_image(img) {
    img.onload = _ => draw(img);
};

function draw(img) {
    canvas.style.borderColor = '#000313';
    const [w, h] = [img.width, img.height];
    [canvas.width, canvas.height] = [w, h];
    const [ws, hs] = [w / 2, h / 2];
    
    ctx.drawImage(img, 0, 0, ws, hs);

    ctx.translate(0, h);
    ctx.scale(1., -1.);
    ctx.drawImage(img, 0, 0, ws, hs);
    
    ctx.translate(w, 0);
    ctx.scale(-1., 1.);
    ctx.drawImage(img, 0, 0, ws, hs);
    
    ctx.translate(0, h);
    ctx.scale(1., -1.);
    ctx.drawImage(img, 0, 0, ws, hs);
}
