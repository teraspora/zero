BASH
====

- ALT+u – Capitalize all characters in a word after the cursor
- ALT+l – Uncapitalize all characters in a word after the cursor
- ALT+r – Undo any changes to a command from the history
- CTRL+w – Delete the token before the cursor
- CTRL+u - Delete back to start
- CTRL+k - Delete to end (Doesn't work in VSCode shells)
- CTRL+e or END - Move cursor to end
- CTRL+a or HOME - Move cursor to start
- CTRL+xx – Toggle between start  and current cursor position
- ESC+. - Retrieve the rightmost token from the last command
- ALT+. - Cycle backwards through the rightmost tokens of previous commands
- CTRL+r - So-called "reverse-i-search"
- CTRL+z - Move current foreground job to background (Re-foreground it with the 'fg' command)
- CTRL+c - Kill the current process or command
- CTRL+d - Kill a Python shell
- CTRL+V - Paste
- CTRL+C - Copy
- !! – Repeat the last command
- ESC+t – Swaps the last two words
- CTRL+f - Move forwards one character
- CTRL+b - Move backwards one character
- CTRL+l - Clear the screen except for the current command